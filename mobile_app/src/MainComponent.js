import React, {Fragment, Component} from 'react';
import ImagePicker from 'react-native-image-picker';
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    SafeAreaView
} from 'react-native';
export default class MainComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filepath: {
                data: '',
                uri: ''
            },
            fileData: '',
            fileUri: '',
            imageUploaded: false,
            analyzeInProgress: false,
            result: ""
        }
    }

    renderResult = () =>  {
        if (this.state.analyzeInProgress) {
            return <Text>Analyzing...</Text>
        }
        else if(this.state.result) {
            return <Text>Brand of your car is {this.state.result}</Text>
        }
        else {
            return <Text></Text>
        }
    }

    renderAnalyzeButton = () =>  {
        if (this.state.fileData) {
            return <TouchableOpacity onPress={this.analyzePhoto} style={styles.button}>
                <Text style={styles.buttonText}>Analyze Photo</Text>
            </TouchableOpacity>
        } else {
            return <TouchableOpacity disabled={true} onPress={this.analyzePhoto} style={styles.button_disabled}>
                <Text style={styles.buttonText}>Analyze Photo</Text>
            </TouchableOpacity>
        }
    }


    renderFileData = () =>  {
        if (this.state.fileData) {
            return <Image source={{uri: 'data:image/jpeg;base64,' + this.state.fileData}}
                          style={styles.images}/>
        } else {
            return <Text style={styles.images}>Image Placeholder</Text>
        }
    }

    launchCamera = () => {

        const options = {};
        ImagePicker.launchCamera(options, (response) => {
            this.processPhoto(response);
        });
    }

    fromGallery = () => {

        const options = {};
          ImagePicker.launchImageLibrary(options, (response) => {
           this.processPhoto(response);
        });
    }

    processPhoto = (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
        } else {

            this.setState({
                filePath: response,
                fileData: response.data,
                fileUri: response.path
            });
        }
    }

    analyzePhoto = () => {
        this.setState({
            analyzeInProgress: true
        });

        const newImageUri =  "file:///" + this.state.fileUri.split("file:/").join("");

        const photo = {
            uri: newImageUri,
            type: 'image/jpeg',
            name: this.state.fileUri
        };
        console.log(photo)
        const form = new FormData();
        form.append('file', photo);

        //need be my local ip
        fetch('http://192.168.1.4:5000/', {
            method: 'POST',
            body: form,
             headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d',
            'Content-Language': 'en'
          }

        })
            .then(result => result.json())
            .then(response =>
                this.setState( {
                   result: response.brand
                }))
            .catch((error) =>
            console.log("Problem while analyzing " + error)
            )
            .finally(() => this.setState({
                analyzeInProgress: false
            }));
        console.log('User is trying to analyze car photo');
    }

    render() {

        return (
            <Fragment>
                <SafeAreaView>
                    <View style={styles.body}>
                        {this.renderFileData()}
                        <Text style={styles.text}>Car Photo</Text>
                        <View style={styles.buttonsContainer}>
                            < TouchableOpacity onPress={() => this.launchCamera()} style={styles.button}>
                                <Text style={styles.buttonText}
                                >Take a photo</Text>
                            </TouchableOpacity>
                            < TouchableOpacity onPress={() => this.fromGallery()} style={styles.button}>
                                <Text style={styles.buttonText}
                                >Pick from Gallery</Text>
                            </TouchableOpacity>
                        </View>
                        {this.renderAnalyzeButton()}
                        {this.renderResult()}
                    </View>
                </SafeAreaView>
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    images: {
        width: 200,
        height: 200,
        borderColor: 'black',
        borderWidth: 2,
        marginHorizontal: 5,
        textAlign: 'center',
    },
    body: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlign: 'center',
        marginTop: 10
    },
    buttonsContainer: {
        marginLeft: 20,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button: {
        backgroundColor: '#248433',
        width: '30%',
        margin: 10,
    },
    button_disabled: {
        backgroundColor: '#1d5529',
        width: '30%',
        margin: 10,
    },
    buttonText: {
        textAlign: 'center',
    }
});

