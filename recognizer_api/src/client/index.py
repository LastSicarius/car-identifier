from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS
from PIL import Image
from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import json
import logging
app = Flask(__name__)
CORS(app)
logging.basicConfig(level=logging.INFO)
row, column = 200, 200


def get_model():
    return load_model("/app/car_model.model")


def resize_image(img):
    img = img.resize((row, column), Image.ANTIALIAS)
    return img


def check_brand(filename):
    model = get_model()
    img = image.load_img(filename, target_size=(column, row))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x /= 255.
    images = np.vstack([x])
    logging.info(images.shape)
    classes = model.predict(images)

    with open('/app/brands.txt', 'r') as filehandle:
        brands = json.load(filehandle)

    return brands[np.argmax(classes, axis=1)[0]]


@app.route("/", methods=['POST'])
def get_brand():
    data = request.files['file']
    data.save("img.jpg")
    logging.info(data)
    return jsonify({"brand": check_brand("img.jpg")})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("5000"), debug=False, threaded=False)
