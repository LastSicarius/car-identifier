from PIL import Image
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
import numpy as np
import logging
import json
from keras_preprocessing.image import img_to_array

row, column = 200, 200
logging.basicConfig(level=logging.INFO)


def get_model():
    return load_model("/app/car_model.model")


def resize_image(img):
    img = img.resize((row, column), Image.ANTIALIAS)
    return img


def get_brands():
    with open('../../brands.txt', 'r') as filehandle:
        return json.load(filehandle)


def normalize(picture):
    width, height = picture.size
    normalized_array = []
    for j in range(0, height):
        for i in range(0, width):
            pixel = picture.getpixel((i, j))
            normalized_array.append(pixel[0] / 255.0)
    return np.array(normalized_array)


def check_brand(filename):
    model = get_model()
    img = image.load_img(filename, target_size=(column, row))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x /= 255.
    images = np.vstack([x])
    logging.info(images.shape)
    classes = model.predict(images)

    with open('../../brands.txt', 'r') as filehandle:
        brands = json.load(filehandle)

    return brands[np.argmax(classes, axis=1)[0]]
