from setuptools import setup

setup(
   name='Can Identifier',
   version='1.0',
   description='The neutral network to identify a car brand on image.',
   author='Adam Blicharski',
   author_email='ba39225l@zet.edu.pl',
   packages=['src/client']  #same as name
)
