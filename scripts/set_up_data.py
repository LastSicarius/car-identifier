import numpy as np
import os
import json

from keras.utils import np_utils
import cv2
from sklearn.utils import shuffle

row, column = 200, 200
brands = []
X_data = []
Y_data = []

np.random.seed(0)


def label_img(img):
    word_label = img.split('.')[0]
    for brand_name, brand_number in brands:
        if brand_name in word_label:
            return brand_number
        else:
            continue


def create_data_set():
    test_dir = 'E:\\dataset_RGB\\'
    for i in os.listdir(test_dir):
        for image in os.listdir(test_dir + i):
            label = label_img(image)
            path = os.path.join(test_dir + i, image)
            try:
                img = cv2.imread(path)
                img = set_image(img, path)
                X_data.append(img)
                Y_data.append(label)

            except Exception as e:
                print(str(e))


def set_image(img, path):
    resize_img = cv2.resize(img, (row, column))
    # cv2.imwrite(path, resize_img)
    return resize_img


def populate_brands():
    test_dir = 'E:\\dataset_RGB'
    for num, dir_name in enumerate(os.listdir(test_dir)):
        brands.append((dir_name, num))


populate_brands()

classes = len(brands)

create_data_set()

total_input = len(X_data)
print("Total Data : %d" % total_input)


print(classes)


# train_data = np.load('C:\\Users\\Adam\\inzynierka\\project\\scripts\\training_data.npy', allow_pickle=True)
# print(train_data[100][1])
# for data in train_data:
#     print(data[1])
total_input = len(Y_data)
X_data = np.array(X_data)
X_data = X_data.astype('float32')
X_data /= 255
Y_data = np.array(Y_data)
Y_data = Y_data.reshape(total_input, 1)

Y_data = np_utils.to_categorical(Y_data, classes)

X_data, Y_data = shuffle(X_data, Y_data)

print(X_data.shape)

np.save('X_data.npy', X_data)
np.save('Y_data.npy', Y_data)

with open('brands.txt', 'w') as filehandle:
    json.dump(brands, filehandle)
