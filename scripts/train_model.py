import numpy as np
from keras.layers import Dense, Flatten, Dropout
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.models import Sequential
import keras
from keras import backend as k
from keras.models import load_model


row, column = 200, 200
classes = 10
batch_size = 64
epochs = 1


def test_data(array, number):
    return array[number]


def train_data(array, number):
    result = []
    for index, ar in enumerate(array):
        if index == number:
            continue
        else:
            result.append(array[index])
    result = np.concatenate(result)
    return result


def create_model():
    model_keras = Sequential()
    model_keras.add(Conv2D(64, (3, 3),
                           input_shape=(200, 200, 3),
                           strides=(1, 1),
                           padding='valid',
                           activation='relu'))
    model_keras.add(MaxPooling2D(pool_size=(2, 2)))
    model_keras.add(Conv2D(32, (3, 3),
                           strides=(1, 1),
                           padding='valid',
                           activation='relu'))
    model_keras.add(MaxPooling2D(pool_size=(2, 2)))

    model_keras.add(Flatten())
    model_keras.add(Dense(98, activation='relu'))
    model_keras.add(Dropout(0.25, name='dropout_1'))
    model_keras.add(Dense(10, activation='softmax'))

    model_keras.compile(loss=keras.losses.categorical_crossentropy,
                        optimizer=keras.optimizers.Adadelta(),
                        metrics=['accuracy'])
    model_keras.summary()
    return model_keras


def train_and_test(number, model_to_train, train_data_x, train_data_y, test_data_x, test_data_y):
    print("Run " + str(number))
    model_to_train.fit(train_data_x, train_data_y, batch_size=batch_size, epochs=epochs, verbose=1)
    # Test
    score = model_to_train.evaluate(test_data_x, test_data_y, verbose=1)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    model_name = "car_model" + str(number) + ".model"
    model_to_train.save(model_name)
    k.clear_session()
    return score[0], score[1], model_name


def pick_best(train_results):
    maximum = 0
    best_model_name = ""
    for loss, test_acc, model_name in train_results:
        if test_acc > maximum:
            maximum = test_acc
            best_model_name = model_name

    return load_model(best_model_name)


data_x = np.load('C:\\Users\\Adam\\inzynierka\\project\\scripts\\X_data.npy', allow_pickle=True)
data_y = np.load('C:\\Users\\Adam\\inzynierka\\project\\scripts\\Y_data.npy', allow_pickle=True)

x_arrays = np.array_split(data_x, 5)
y_arrays = arrays = np.array_split(data_y, 5)

i = 0
results = []
while i < 5:
    results.append(train_and_test(i + 1, create_model(), train_data(x_arrays, i), train_data(y_arrays, i),
                                  test_data(x_arrays, i),
                                  test_data(y_arrays, i)))
    i += 1

print(results)
best_model = pick_best(results)
best_model.save("car_model.model")

print(data_x.shape)
