# Script to rename all files in base directory.
# Helpful with rename names of images for given class-brand.

import os
path = 'E:\\dataset_RGB1\\'

for brand_dir in os.listdir(path):
	dirPath = os.path.join(path, brand_dir)
	files = os.listdir(dirPath)
	for num, file in enumerate(files):
		oldext = os.path.splitext(file)[1]  # keep old extension
		os.rename(os.path.join(dirPath, file), os.path.join(dirPath, brand_dir) + str(num) + oldext)
